# Extreme programming

## Clojure

Dans ce défi, vous devez créer un programme qui prend en entrée le fichier text.txt qui affiche une structure de données associant chaque lettre présente dans le texte et le nombre d'occurrences de celle-ci.

### Contraintes
* Votre programme doit utiliser la fonction map et reduce de clojure.
* Votre programme doit être exécutable en roulant un fichier s'appelant main.clj
