package com.sag0ld.csgamemeteo;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;
import org.json.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    ProgressBar loading;
    TextView cityName;
    TextView currentDegree;
    TextView minDegree;
    TextView maxDegree;
    ImageView currentWeatherImg;
    Time today ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loading = findViewById(R.id.progressBar);
        cityName = findViewById(R.id.cityName);
        currentDegree = findViewById(R.id.currentDegree);
        minDegree = findViewById(R.id.minDegree);
        maxDegree = findViewById(R.id.maxDegree);
        today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        currentWeatherImg = findViewById(R.id.weatherImage);
        new GetMeteoData(this).execute();
    }

    public class GetMeteoData extends AsyncTask<Void, Void, String> {

        private Exception exception;
        Context context;
        GetMeteoData (Context p_context) {
            context = p_context;
        }

        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            cityName.setText("");
        }

        protected String doInBackground(Void... string ) {
            String city = "montréal";
            // Do some validation here
            StringBuilder stringBuilderCityInfo;
            try {
                URL url = new URL("https://www.metaweather.com/api/location/search/?query="+city);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    stringBuilderCityInfo = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilderCityInfo.append(line).append("\n");
                    }
                    bufferedReader.close();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
            String cityId = "3534";
            // get the json object of the city
            try {
                JSONArray object = new JSONArray(stringBuilderCityInfo.toString());
                JSONObject json = object.getJSONObject(0);
                cityName.setText(json.get("title").toString());
                cityId = json.get("woeid").toString();
            } catch (Exception e){
                Log.i("Exception", e.getMessage());
            }


            try {
                URL url2 = new URL("https://www.metaweather.com/api/location/"+cityId+"/"+today.year+"/"+today.month+"/"+today.monthDay+"/");
                HttpURLConnection urlConnection = (HttpURLConnection) url2.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String p_response) {
            if (p_response == null) {
                p_response = "THERE WAS AN ERROR";
            }
            loading.setVisibility(View.GONE);
            try {
                JSONArray object = new JSONArray(p_response);
                JSONObject json = object.getJSONObject(0);
                Double current = Math.ceil(Double.parseDouble(json.get("the_temp").toString()));
                currentDegree.setText(current.toString());
                Double min = Math.ceil(Double.parseDouble(json.get("min_temp").toString()));
                minDegree.setText(min.toString()+" / ");
                Double max = Math.ceil(Double.parseDouble(json.get("max_temp").toString()));
                maxDegree.setText(max.toString());

                String acroImg = json.get("weather_state_abbr").toString();

                String urlImg = "https://www.metaweather.com/static/img/weather/png/"+acroImg+".png";
                Picasso.with(MainActivity.this).load(urlImg)
                        .resize(300, 300)
                        .centerCrop().into(currentWeatherImg);
            } catch (Exception e){
                Log.i("Exception", e.getMessage());
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
